// Importing required dependencies

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlTemplateConfig = require('./config/template');

// Defining server port

const APP_PORT = 9000;

const scripts = [
    'https://www.clickbus.com.br/static/js/vendors.min.js?v=0.1-700',
    'https://www.clickbus.com.br/static/js/commons.min.js?v=0.1-700',
    'https://www.clickbus.com.br/static/js/commons.min.js?v=0.1-700'
];

scripts.forEach(script => {
    HtmlTemplateConfig.scripts.push({
        src: script
    });
});

// Inserting ClickBus CSS

HtmlTemplateConfig.links.push({
    rel: 'stylesheet',
    href: 'https://www.clickbus.com.br/static/css/brazil.min.css?v=0.1-700'
});

// Webpack Dev configuration

module.exports = merge(common, {
    mode: 'development',
    optimization: {
        minimize: false
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        index: 'index.html',
        compress: false,
        port: APP_PORT
    },
    plugins: [
        new HtmlWebpackPlugin(HtmlTemplateConfig)
    ]
});