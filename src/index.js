import 'regenerator-runtime/runtime';


import { buildDates, updateClickBusURLBasedOnDate } from './components/utils';
import CreateAccordions from "./components/accordion";
import { sendNewsletter } from "./components/newsletter";

import './assets/scss/app.scss';

const landingPageContent = document.getElementById('landing-page-content');

const runLandingPageApplication = () => {
    document.addEventListener('DOMContentLoaded', () => {
        const salesForceSuccessUrlInput = document.querySelector('[name="_successURL"]');
        const salesForceErrorUrlInput = document.querySelector('[name="_errorURL"]');
        const availableDates = document.querySelectorAll('.available-dates');
        const cardList = document.getElementById('list-card-inline-offers');

        const location = window.location.href;
        const firstPartURL = location.split('?')[0];

        const params = new URLSearchParams(window.location.search);
        const hasSuccessInParams = params.has('success');

        if(hasSuccessInParams) {
            const successAsString = params.get('success').toString();
            params.delete('success');
            window.location.href = firstPartURL;
            return successAsString === 'true' ?
                alert('Cadastrado com sucesso. A ClickBus agradece') :
                alert('Houve um erro ao cadastrar. Por favor tente novamente.')
        }

        // Updating callback urls
        salesForceSuccessUrlInput.value = `${firstPartURL}?success=true`;
        salesForceErrorUrlInput.value = `${firstPartURL}?success=false`;

        // Display dates
        buildDates(availableDates);

        // Updating date on select change

        availableDates.forEach((availableDate => {
            availableDate.addEventListener('change', () => {
                const option = availableDate.value;
                const row = availableDate.getAttribute('data-card-position');
                const cardRow = cardList.querySelectorAll('.card-offer')[row - 1];
                const cardLink = cardRow.querySelector('a');
                updateClickBusURLBasedOnDate(option, cardLink);
            })
        }))

        // Accordions
        CreateAccordions();

        // Cards
        document.getElementById('list-card-inline-offers').querySelectorAll('.card-offer').forEach(card => {
            card.addEventListener('click', () => {
                card.classList.toggle('opened');
            });
        });

        document.getElementById('list-card-inline-offers').querySelectorAll('.offer-options').forEach(option => {
            option.addEventListener('click', e => e.stopImmediatePropagation());
        });

        // Newsletter

        // CBXNewsletterForm.addEventListener('submit', async (e) => {
        //     e.preventDefault();
        //     submitButton.textContent = 'Enviando...';
        //
        //     const email = document.getElementById('cbx-email-lead');
        //     const success = await sendNewsletter(email);
        //
        //     if(success) {
        //         CBXNewsletterForm.reset()
        //         submitButton.textContent = 'Enviado!';
        //         setTimeout(() => submitButton.textContent = 'Enviar', 3000);
        //         return;
        //     }
        //
        //     submitButton.textContent = 'Enviar'
        // })
    });
}

if(!landingPageContent) {
    throw new Error('The page\'s main wrapper could not be found. Please wrap the entire application within an element with id "#landing-page-content". So we guarantee that we will not have any kind of conflict.')
}

runLandingPageApplication();