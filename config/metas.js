// Array above contains all meta tags that will be rendered in final LP code
// To use it just create a new object inside array with a couple of attributes. Generally name and content

module.exports = [
    {
        name: 'og:url',
        content: 'ClickBus X: O jeito mais fácil, barato e seguro de viajar.'
    },

    {
        name: 'description',
        content: 'ClickBus X: O jeito mais fácil, barato e seguro de viajar.'
    },
]