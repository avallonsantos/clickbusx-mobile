const path = require('path')
// Importing meta tags for this landing page
const Metas = require('./metas');

// Exporting literal object with configuration of HTML Webpack Plugin

module.exports = {
    links: [],
    scripts: [],
    title: 'ClickBus X | O jeito mais fácil, barato e seguro de viajar.',
    mobile: true,
    lang: 'pt_BR',
    template: path.resolve(__dirname, './../src/static/index.html'),
    meta: Metas,
    renderPlaceholders: false
};